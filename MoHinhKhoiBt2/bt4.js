/* Input : 

chiều dài : 10cm
chiều rộng : 5cm

Các bước xử lí

Ct tính diện tích HCN

chiều dài * chiều rộng

Ct tính chu vi HCN

(chiều dài + chiều rộng)*2

output 

Diện tích= ? cm
chu vi= ? cm
*/
// chiều rộng : txt-chieu-rong;
// chiều dài : txt-chieu-dai;
// diện tích, chu vi : txt-resuldtcv;


function tinhDTCV(){
    var chieuDaiValue = document.getElementById("txt-chieu-dai").value * 1;
    console.log("chieuDaiValue",chieuDaiValue); 
    var chieuRongValue = document.getElementById("txt-chieu-rong").value * 1;
    console.log("chieuRongValue",chieuRongValue);
    var dienTichValue = chieuDaiValue * chieuRongValue;
    var chuViValue = (chieuDaiValue + chieuRongValue) * 2;
    console.log("dienTichValue",dienTichValue,"chuViValue",chuViValue);

    document.getElementById("txt-resuldtcv").value = ` Diện tích : ${dienTichValue}  Chu vi :  ${chuViValue} ` ;
}
  